package dealership.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.model.Offer;

class OfferTests {
	
	private Offer o = new Offer(null, null, null, null);
	String VIN = "QWERTY";
	double offer = 2000.99;
	String status = "pending";

	@Test
	void testGetVIN() {
		o.setVIN(VIN);
		assertEquals(VIN, o.getVIN());
	}

	@Test
	void testGetPrice() {
		o.setPrice(offer);
		assertEquals(offer, o.getPrice());
	}

	@Test
	void testGetStatus() {
		o.setStatus(status);
		assertEquals(status, o.getStatus());
	}

}
