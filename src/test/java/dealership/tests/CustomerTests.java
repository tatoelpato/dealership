package dealership.tests;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.Before;
import org.junit.jupiter.api.Test;

import com.model.Customer;

class CustomerTests {
	
	private Customer c = new Customer(null, null);
	String username = "zxc";
	String password = "zxc";
	
	@Test
	void testGetUsername() {
		c.setPassword(username);
		assertEquals(username, c.getPassword());
	}

	@Test
	void testGetPassword() {
		c.setPassword(password);
		assertEquals(password, c.getPassword());
	}

}
