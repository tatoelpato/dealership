package dealership.tests;

import static org.junit.jupiter.api.Assertions.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import org.hamcrest.collection.IsMapContaining;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.junit.jupiter.api.Test;

import com.model.Car;

import junit.framework.Assert;

class InventoryTests {
	
	private Car c = new Car(2020, "Honda", "CRV", 0, 25999.99, "QWERTY", "White");
	private String file = "./src/main/java/com/data/CarLot.txt";
	private Map<String, Car> car = new TreeMap<>();
	
	@Test
	void testAddVehicle() {
		car.put("QWERTY", c);
		assertThat(car, IsMapContaining.hasKey("QWERTY"));
		
	}

	@Test
	void testSaveVehicle() {
		
	}

	@Test
	void testLoadVehicle() {
		fail("Not yet implemented");
	}

	@Test
	void testPrintCar() {
		fail("Not yet implemented");
	}

	@Test
	void testRemoveCar() {
		car.put("QWERTY", c);
		car.remove("QWERTY");
		 assertThat(car, not(IsMapContaining.hasEntry("QWERTY", c)));
		
	}

	@SuppressWarnings("deprecation")
	@Test
	void testVinExists() {
		car.put("QWERTY", c);
		Assert.assertTrue(car.containsKey("QWERTY"));
	}

}
