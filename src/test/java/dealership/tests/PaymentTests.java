package dealership.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.model.Payment;

class PaymentTests {
	
	private Payment p = new Payment(null, null, 0, null, null);
	String VIN = "QWERTY";
	String username = "zxc";
	double payment = 200.99;
	String manufacturer = "Toyota";
	String model = "Corolla";

	@Test
	void testSetVIN() {
		p.setVIN("QWERTY");
		assertEquals(VIN, p.getVIN());
	}

	@Test
	void testSetUsername() {
		p.setUsername(username);
		assertEquals(username, p.getUsername());
	}

	@Test
	void testSetPayment() {
		p.setPayment(payment);
		assertEquals(payment, p.getPayment());
	}

	@Test
	void testSetManufacturer() {
		p.setManufacturer(manufacturer);
		assertEquals(manufacturer, p.getManufacturer());
	}
	
	@Test
	void testSetModel() {
		p.setModel(model);
		assertEquals(model, p.getModel());
	}

	@Test
	void testToString() {
		String expected = "Customer [username=null, password=null]";
		assertEquals(expected, p.toString());
	}

}
