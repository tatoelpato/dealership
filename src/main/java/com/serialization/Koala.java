package com.serialization;

import java.io.Serializable;
import java.util.Scanner;

import com.model.CreateUser;
import com.model.Customer;
import com.model.Employee;

import services.CustomerServices;
import services.EmployeeServices;

public class Koala implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final Scanner input = new Scanner(System.in);
	private CreateUser cU = new CreateUser();

	public Koala() {
		cU.loadCustomer();
		cU.loadEmployee();
	}

	public void launch() {
		boolean valid = true;
		while (valid) {
			System.out.println("   __                      __");
			System.out.println(".-'  `'.._...-----..._..-'`  '-.");
			System.out.println("/                               \\");
			System.out.println("|  ,   ,'                '.  ,  |");
			System.out.println("\\  '-/                    \\-'  /");
			System.out.println(" '._|          _           |_.'");
			System.out.println("    |    /\\   / \\    /\\    |");
			System.out.println("    |    \\/   | |    \\/    |");
			System.out.println("     \\        \\\"/         /");
			System.out.println("      '.    =='^'==     .'");
			System.out.println("        `'------------'`");
			
			System.out.println("---Hello There---\n" + "Please choose one of the following: "
					+ "\n1. Create Account\n" + "2. Login\n" + "3. Exit the app");
			int choice = Koala.input.nextInt();
			switch (choice) {
			case 1:
				System.out.println("\nCreate Account");
				newUser();
				valid = true;
				break;
			case 2:
				System.out.println("\nLogin");
				System.out.println("1.Customer Login");
				System.out.println("2.Employee Login");
				choice = Koala.input.nextInt();
				switch (choice) {
				case 1:
					customerLogin();
					valid = true;
					break;
				case 2:
					employeeLogin();
					valid = true;
					break;
				}
				break;
			default:
				System.out.println("Goodbye!");
				valid = false;
				break;
			}
		}

	}

	private void employeeLogin() {
		boolean valid = true;
		while (valid) {
			System.out.print("Username: ");
			String username = Koala.input.next();
			System.out.print("password: ");
			String password = Koala.input.next();
			cU = new CreateUser();
			cU.loadEmployee();
			Employee e;
			if ((e = cU.employeeLogin(username, password)) != null) {
				if (e.getUsername().equals(username) && e.getPassword().equals(password)) {
					employeeMenu(e.getUsername());
					valid = false;
				}
			} else {
				System.out.println("ERROR!");
			}
		}
	}

	private void customerLogin() {
		boolean valid = true;
		while (valid) {
			System.out.print("Username: ");
			String username = Koala.input.next();
			System.out.print("password: ");
			String password = Koala.input.next();
			cU = new CreateUser();
			cU.loadCustomer();
			Customer c;
			if ((c = cU.customerLogin(username, password)) != null) {
				if (c.getUsername().equals(username) && c.getPassword().equals(password)) {
					customerMenu(c.getUsername());
					valid = false;
				}
			} else {
				System.out.println("ERROR!");
			}
		}
	}

	@SuppressWarnings("static-access")
	private void customerMenu(String username) {
		CustomerServices cS = new CustomerServices();
		System.out.println("\n---Customer Menu--");
		cS.customerServices(username);
	}

	@SuppressWarnings("static-access")
	private static void employeeMenu(String username) {
		EmployeeServices eS = new EmployeeServices();
		System.out.println("\n---Employee Menu--");
		eS.employeeServices(username);
	}

	private void newUser() {
		boolean valid = true;
		while (valid) {
			Customer c;
			Employee e;
			System.out.print(
					"\nWhich type of account would you like to create?\n" + "\n1. Customer" + "\n2. Employee\n");
			int choice = Koala.input.nextInt();
			String username = "";
			String password = "";
			boolean redo = true;
			switch (choice) {
			case 1:
				username = "";
				while (redo) {
					System.out.println("Create your username: ");
					username = input.next();
					if (cU.customerExists(username)) {
						System.out.println("Username is not available");
						launch();
						redo = true;
					}
					redo = false;
				}
				System.out.println("Create a password: ");
				password = Koala.input.next();
				c = new Customer(username, password);
				System.out.println("Welcome! Account has been created.");
				System.out.println("Happy Car Shopping!\n");
				cU.addCustomer(c);
				cU.saveCustomer();
				valid = false;
				break;
			case 2:
				username = "";
				while (redo) {
					System.out.println("Create your username: ");
					username = Koala.input.next();
					if (cU.employeeExists(username)) {
						System.out.println("Here");
						System.out.println("Username is not available");
						launch();
						redo = true;
					}
					redo = false;
				}
				System.out.println("Create a password: ");
				password = Koala.input.next();
				e = new Employee(username, password);
				System.out.println("Welcome to the Koala Team!");
				System.out.println("Where only the KOALAfied are selected");
				cU.addEmployee(e);
				cU.saveEmployee();
				valid = false;
				break;
			}
		}
	}

}
