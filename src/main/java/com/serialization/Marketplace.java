package com.serialization;

import java.util.Map;

import com.model.Car;
import com.model.CreateCustomerLot;
import com.model.CreateOffer;
import com.model.CreatePayment;
import com.model.CustomerLot;
import com.model.Inventory;
import com.model.Offer;
import com.model.Payment;

import services.CustomerServices;

public class Marketplace {

	private static CreateOffer cO = new CreateOffer();
	private static CreatePayment cP = new CreatePayment();
	private static Map<String, Offer> offer = cO.loadOffer();
	private static Inventory i = new Inventory();
	private static Map<String, Car> car = i.loadVehicle();
	private static CreateCustomerLot cL = new CreateCustomerLot();
	private static Map<String, CustomerLot> customerCarLot = cL.loadCustomerCar();

	public static void market() {
		int menuOption = 0;
		System.out.println("Welcome to the Dealership please pick from the following options (Press 0 for menu)");
		displayMenu();
		do {
			System.out.println("\n\nWhat would you like to do? (0 for main menu)");
			menuOption = Koala.input.nextInt();
			while (menuOption < 0 || menuOption > 7) {
				System.out.println("Please enter a correct menu option.(0 to see the menu)");
				menuOption = Koala.input.nextInt();
			}
			doOption(menuOption);

		} while (menuOption != 7);
	}

	public static void displayMenu() {
		System.out.println("\n1. Make an offer");
		System.out.println("2. Make a payment");
		System.out.println("3. Cancel offer");
		System.out.println("4. See my offer");
		System.out.println("5. Add car to lot");
		System.out.println("6. Calculate montly payments");
		System.out.println("7. Exit");
	}

	public static void doOption(int menu) {
		switch (menu) {
		case 0:
			System.out.println("Main Menu");
			displayMenu();
			break;
		case 1:
			System.out.println("Make an offer");
			makeOffer();
			break;
		case 2:
			System.out.println("Make a payment");
			pay();
			break;
		case 3:
			System.out.println("Cancel offer");
			cancelOffer();
			break;
		case 4:
			System.out.println("See my offer's status");
			viewMyOffer();
			break;
		case 5:
			System.out.println("Add car");
			addCarCustomerLot();
		case 6:
			System.out.println("Calculate payments");
			calculator();
		default:
			System.out.println("Return to main menu");
			break;
		}
	}

	private static void calculator() {
		cP.calculator();
		
	}

	private static void addCarCustomerLot() {
		viewMyOffer();
		boolean validInput;
		String VIN;
		String username;
		String manufacturer;
		String model;
		double value = 0.0;
		do {
			viewMyPurchase();
			validInput = true;
			System.out.println("Enter the car's VIN: (Must be 6 alpahnumeric characters) ");
			VIN = Koala.input.next().toUpperCase();
			VIN = VIN.toUpperCase();
			if (VIN.length() != 6) {
				validInput = false;
				System.out.println("VIN must be 6 characters");
			}
			System.out.println("Enter your username: ");
			username = Koala.input.next();
			System.out.println("Enter the car's manufacturer: ");
			manufacturer = Koala.input.next();
			System.out.println("Enter the car's model: ");
			model = Koala.input.next();
			System.out.println("Enter the car's value: ");
			if (Koala.input.hasNextDouble())
				value = Koala.input.nextDouble();
			else
				validInput = false;
			if (!validInput) {
				System.out.println("\nIncorrect Format");
				System.out.println("Please try again\n");
			}
		} while (validInput == false);
		cL.addtoCustomerCarLot(new CustomerLot(VIN, username, manufacturer, model, value));
		cL.saveCustomerCar();
		System.out.println("Your purchase has been completed!");
	}

	public static void viewMyCar() {
		System.out.println("What is the VIN of your vehicle?");
		String VIN = Koala.input.next();
		System.out.println("Please enter your username: ");
		String username = Koala.input.next();
		if (customerCarLot.get(VIN).getUsername().equals(username)) {
			CustomerLot cc = customerCarLot.get(VIN);
			System.out.println(cc.toString());
		}
	}

	public static void viewMyPurchase() {
		System.out.println("Please enter the VIN of the car you are purchasing");
		String VIN = Koala.input.next();
		if (car.containsKey(VIN)) {
			Car c = car.get(VIN);
			System.out.println(c.toString());
		}
	}

	private static void viewMyOffer() {
		System.out.println("Please enter your username");
		String username = Koala.input.next();
		if (offer.containsKey(username)) {
			Offer val = offer.get(username);
			System.out.println(val.toString());
		}
	}

	@SuppressWarnings("unused")
	private static void cancelOffer() {
		boolean validInput;
		String username;
		String VIN;
		double price = 0.0;
		String status;
		do {
			validInput = true;
			System.out.println("Enter your username: ");
			username = Koala.input.next();
			System.out.println("Enter the VIN of the car you wish to cancel offer: ");
			VIN = Koala.input.next();
			System.out.println("How much is your offer?: ");
			if (Koala.input.hasNextDouble())
				price = Koala.input.nextInt();
			else
				validInput = false;
			System.out.println("Type cancel to delete it: ");
			status = Koala.input.next();
			if (!validInput) {
				System.out.println("\nIncorrect Format");
				System.out.println("Please try again\n");
			}
		} while (validInput == false);
		cO.removeOffer(username);
		cO.saveOffer();
		System.out.println("Your offer has been canceled!");
	}

	private static void pay() {
		boolean validInput;
		String VIN;
		String username;
		double payment = 0.0;
		String manufacturer;
		String model;
		do {
			validInput = true;
			System.out.println("Enter the VIN of the car you are making a payment for: ");
			VIN = Koala.input.next();
			System.out.println("Enter your username: ");
			username = Koala.input.next();
			System.out.println("How much is your payment?: ");
			if (Koala.input.hasNextDouble())
				payment = Koala.input.nextInt();
			else
				validInput = false;
			System.out.println("Enter the make of the car: ");
			manufacturer = Koala.input.next();
			System.out.println("Enter the model of the car: ");
			model = Koala.input.next();
			if (!validInput) {
				System.out.println("\nIncorrect Format");
				System.out.println("Please try again\n");
			}
		} while (validInput == false);
		cP.makePayment(new Payment(VIN, username, payment, manufacturer, model));
		cP.savePayment();
		System.out.println("Payment has been sent!");
	}

	private static void makeOffer() {
		boolean validInput;
		String username;
		String VIN;
		double price = 0.0;
		String status;
		do {
			validInput = true;
			CustomerServices.showCars();
			System.out.println("Enter your username: ");
			username = Koala.input.next();
			System.out.println("Enter the VIN of the car you wish to make an offer for: ");
			VIN = Koala.input.next();
			System.out.println("How much is your offer?: ");
			if (Koala.input.hasNextDouble())
				price = Koala.input.nextInt();
			else
				validInput = false;
			System.out.println("Type pending to set it: ");
			status = Koala.input.next();
			if (!validInput) {
				System.out.println("\nIncorrect Format");
				System.out.println("Please try again\n");
			}
		} while (validInput == false);
		cO.makeOffer(new Offer(username, VIN, price, status));
		cO.saveOffer();
		System.out.println("Your offer has been placed!");
	}

}
