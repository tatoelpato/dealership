package com.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Map;
import java.util.TreeMap;

public class CreateUser {

	private String customerFile = "./src/main/java/com/data/Customers.txt";
	private Map<String, Customer> customerAccount;
	private String employeeFile = "./src/main/java/com/data/Employees.txt";
	private Map<String, Employee> employeeAccount;
	private ObjectOutputStream oos;
	private ObjectInputStream ois;

	public CreateUser() {
		super();
		customerAccount = new TreeMap<String, Customer>();
		employeeAccount = new TreeMap<String, Employee>();
		File cF = new File(customerFile);
		if (!cF.exists()) {
			try {
				cF.createNewFile();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		loadCustomer();
		File eF = new File(employeeFile);
		if (!eF.exists()) {
			try {
				eF.createNewFile();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		loadEmployee();
	}

	public Customer customerLogin(String username, String password) {
		if (customerAccount.containsKey(username)) {
			Customer c = customerAccount.get(username);
			if (c.getPassword().equals(password)) {
				return c;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	public Employee employeeLogin(String username, String password) {
		if (employeeAccount.containsKey(username)) {
			Employee e = employeeAccount.get(username);
			if (e.getPassword().equals(password)) {
				return e;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	public boolean customerExists(String username) {
		System.out.println("Here");
		return customerAccount.containsKey(username);
	}

	public boolean employeeExists(String username) {
		return employeeAccount.containsKey(username);
	}

	public boolean addCustomer(Customer c) {
		if (customerAccount.containsKey(c.getUsername())) {
			return false;
		} else {
			customerAccount.put(c.getUsername(), c);
			return true;
		}
	}

	public boolean addEmployee(Employee e) {
		if (employeeAccount.containsKey(e.getUsername())) {
			return false;
		} else {
			employeeAccount.put(e.getUsername(), e);
			return true;
		}
	}

	public void saveCustomer() {
		try {
			oos = new ObjectOutputStream(new FileOutputStream(customerFile));
			oos.writeObject(customerAccount);
			oos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void saveEmployee() {
		try {
			oos = new ObjectOutputStream(new FileOutputStream(employeeFile));
			oos.writeObject(employeeAccount);
			oos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@SuppressWarnings("unchecked")
	public void loadCustomer() {
		try {
			ois = new ObjectInputStream(new FileInputStream(customerFile));
			customerAccount = (Map<String, Customer>) ois.readObject();
			ois.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@SuppressWarnings("unchecked")
	public void loadEmployee() {
		try {
			ois = new ObjectInputStream(new FileInputStream(employeeFile));
			employeeAccount = (Map<String, Employee>) ois.readObject();
			ois.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public String printCustomer(String key) {
		return customerAccount.get(key).toString();
	}

	public String printEmployee(String key) {
		return employeeAccount.get(key).toString();
	}

}
