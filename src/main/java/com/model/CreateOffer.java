package com.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class CreateOffer {

	private String offerFile = "./src/main/java/com/data/Offers.txt";
	private Map<String, Offer> offer;
	private ObjectInputStream ois;
	private ObjectOutputStream oos;

	public CreateOffer() {
		super();
		offer = new TreeMap<String, Offer>();
		File ol = new File(offerFile);
		if (!ol.exists()) {
			try {
				ol.createNewFile();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		loadOffer();
	}

	public boolean makeOffer(Offer o) {
		if (offer.containsKey(o.getUsername())) {
			return false;
		} else {
			offer.put(o.getUsername(), o);
			return true;
		}
	}

	public void saveOffer() {
		try {
			oos = new ObjectOutputStream(new FileOutputStream(offerFile));
			oos.writeObject(offer);
			oos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public Map<String, Offer> loadOffer() {
		try {
			ois = new ObjectInputStream(new FileInputStream(offerFile));
			offer = (Map<String, Offer>) ois.readObject();
			ois.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return offer;
	}

	public String printOffer(String key) {
		return offer.get(key).toString();
	}

	public boolean removeOffer(String username) {
		if (offer.containsKey(username)) {
			offer.remove(username);
			return true;
		} else {
			return false;
		}
	}

	public boolean offerExists(String username) {
		return offer.containsKey(username);
	}

	public String printAll() {
		String ret = "";
		Set<String> s = offer.keySet();
		for (String str : s) {
			ret += offer.get(str).toString() + "\n";
		}
		return ret;
	}

	public void accepted(String username) {
		offer.get(username).setStatus("accepted");
		makeOffer(offer.get(username));
		declineAllOthers();
	}

	public void rejected(String username) {
		offer.get(username).setStatus("rejected");
		makeOffer(offer.get(username));
	}

	private void declineAllOthers() {
		offer.forEach((username, o) -> {
			if (!(offer.get(username).getStatus().equals("accepted"))) {
				rejected(o.getUsername());
				System.out.println("all other offers have been rejected");
			}
		});
	}

}
