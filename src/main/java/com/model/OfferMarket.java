package com.model;

import com.serialization.Koala;

public class OfferMarket {
	
	private CreateOffer cO = new CreateOffer();
	
	public void offers() {
		int menuOption = 0;
		System.out.println("Welcome to the Dealership please pick from the following options (Press 0 for menu)");
		displayMenu();
		do {
			System.out.println("\n\nWhat would you like to do? (0 for main menu)");
			menuOption = Koala.input.nextInt();
			while (menuOption < 0 || menuOption > 4) {
				System.out.println("Please enter a correct menu option.(0 to see the menu)");
				menuOption = Koala.input.nextInt();
			}
			doOption(menuOption);

		} while (menuOption != 4);
	}
	
	public void displayMenu() {
		System.out.println("\n1. View all offers");
		System.out.println("2. Accept offer");
		System.out.println("3. Reject offer");
		System.out.println("4. Exit");
	}
	
	public void doOption(int menu) {
		switch (menu) {
		case 0:
			System.out.println("Main Menu");
			displayMenu();
			break;
		case 1:
			System.out.println("All offers");
			allOffers();
			break;
		case 2:
			System.out.println("Accept offer");
			acceptOffer();
			break;
		case 3:
			System.out.println("Decline offer");
			declineOffer();
			break;
		default:
			System.out.println("Return to main menu");
			break;
		}
	}
	
	private void allOffers() {
		System.out.println(cO.printAll());

	}
	
	@SuppressWarnings("unused")
	private void acceptOffer() {
		boolean validInput;
		String username;
		String vin;
		double price = 0.0;
		String status;
		String o;
		do {
			System.out.println(cO.printAll());
			validInput = true;
			System.out.println("Enter the customer's username: ");
			username = Koala.input.next();
			System.out.println("Enter the VIN of the car you are accepting offer for: ");
			vin = Koala.input.next();
			System.out.println("How much is the offer?: ");
			if (Koala.input.hasNextDouble())
				price = Koala.input.nextInt();
			else
				validInput = false;
			System.out.println("Type accepted to approve: ");
			status = Koala.input.next();
			if (!validInput) {
				System.out.println("\nIncorrect Format");
				System.out.println("Please try again\n");
			}
		} while (validInput == false);
		o = cO.printOffer(username);
		cO.accepted(username);
		cO.saveOffer();
		System.out.println("Offer has been approved");
	}
	
	@SuppressWarnings("unused")
	private void declineOffer() {
		boolean validInput;
		String username;
		String vin;
		double price = 0.0;
		String status;
		String o;
		do {
			System.out.println(cO.printAll());
			validInput = true;
			System.out.println("Enter the customer's username: ");
			username = Koala.input.next();
			System.out.println("Enter the VIN of the car you are rejecting offer for: ");
			vin = Koala.input.next();
			System.out.println("How much is the offer?: ");
			if (Koala.input.hasNextDouble())
				price = Koala.input.nextInt();
			else
				validInput = false;
			System.out.println("Type rejected to reject: ");
			status = Koala.input.next();
			if (!validInput) {
				System.out.println("\nIncorrect Format");
				System.out.println("Please try again\n");
			}
		} while (validInput == false);
		o = cO.printOffer(username);
		cO.rejected(username);
		cO.saveOffer();
		System.out.println("Offer has been rejected");
	}

}
