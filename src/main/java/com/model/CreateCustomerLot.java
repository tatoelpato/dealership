package com.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Map;
import java.util.TreeMap;

public class CreateCustomerLot {

	private String customerLot = "./src/main/java/com/data/CustomerLot.txt";
	private Map<String, CustomerLot> customerCarLot;
	private ObjectInputStream ois;
	private ObjectOutputStream oos;
	
	public CreateCustomerLot() {
		customerCarLot=new TreeMap<>();
		File c2 = new File(customerLot);if(!c2.exists())
		{
			try {
				c2.createNewFile();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		loadCustomerCar();
	}


	public boolean addtoCustomerCarLot(CustomerLot c) {
	if (customerCarLot.containsKey(c.getVIN())) {
		return false;
	} else {
		customerCarLot.put(c.getVIN(), c);
		return true;
	}
}

	public void saveCustomerCar() {
	try {
		oos = new ObjectOutputStream(new FileOutputStream(customerLot));
		oos.writeObject(customerCarLot);
		oos.close();
	} catch (Exception e) {
		e.printStackTrace();
	}
}

	@SuppressWarnings("unchecked")
	public Map<String, CustomerLot> loadCustomerCar() {
		try {
			ois = new ObjectInputStream(new FileInputStream(customerLot));
			customerCarLot = (Map<String, CustomerLot>) ois.readObject();
			ois.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return customerCarLot;
	}

	@SuppressWarnings("unlikely-arg-type")
	public boolean printCustomerLot(String username) {
		return customerCarLot.containsValue(username);
	}
	
	@SuppressWarnings("unlikely-arg-type")
	public boolean cutomerCarExists(String username) {
		return customerCarLot.containsValue(username);
	}

}
