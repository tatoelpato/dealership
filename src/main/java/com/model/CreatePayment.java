package com.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import com.serialization.Koala;

public class CreatePayment {

	private String payFile = "./src/main/java/com/data/Payments.txt";
	private Map<String, Payment> payment;
	private ObjectInputStream ois;
	private ObjectOutputStream oos;

	public CreatePayment() {
		super();
		payment = new TreeMap<String, Payment>();
		File p1 = new File(payFile);
		if (!p1.exists()) {
			try {
				p1.createNewFile();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		loadPayment();
	}

	public boolean makePayment(Payment p) {
		if (payment.containsKey(p.getVIN())) {
			return false;
		} else {
			payment.put(p.getVIN(), p);
			return true;
		}
	}

	public void savePayment() {
		try {
			oos = new ObjectOutputStream(new FileOutputStream(payFile));
			oos.writeObject(payment);
			oos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public Map<String, Payment> loadPayment() {
		try {
			ois = new ObjectInputStream(new FileInputStream(payFile));
			payment = (Map<String, Payment>) ois.readObject();
			ois.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return payment;
	}

	public String printPayment(String key) {
		return payment.get(key).toString();
	}

	public boolean applyPayment(String VIN) {
		if (payment.containsKey(VIN)) {
			payment.remove(VIN);
			return true;
		} else {
			return false;
		}
	}

	public boolean paymentExists(String VIN) {
		return payment.containsKey(VIN);
	}

	public String printPayments() {
		String pay = "";
		Set<String> s = payment.keySet();
		for (String p : s) {
			pay += payment.get(p).toString() + "\n";
		}
		return pay;
	}

//	public static void carPayment(double payment,int numYears, String VIN, Payment p, Offer o) {
//	    double carPrice, remainingBalance, monthlyBalance,
//	    		interest, monthlyPayment;
//	    int months;
//	    
//	    Locale locale = new Locale("en", "US");
//	    NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(locale);
//	    
//		if (VIN.equals(p.getUsername())) {	    
//		    carPrice = o.getPrice();
//		    //Preset interestRate to 9.5 if greater than 30k else set to 6.5
//		    double interestRate = (carPrice>30000)?9.5:6.5;
//		    //this check if the user entered an invalid value for the loan length or interest rate
//		    if (numYears <= 0) {
//		      System.out.println("Error! You must take out a valid car loan.");
//		      //goes back to customerMenu
//		    } else if (payment >= carPrice) {
//		    	System.out.println("The car is paid in full.");
//		      } else {
//			      remainingBalance = carPrice - payment;
//			      months = numYears * 12;
//			      monthlyBalance  = remainingBalance / months;
//			      interest = monthlyBalance * interestRate / 100;
//			      monthlyPayment = monthlyBalance + interest;
//			      System.out.println("Your monthly payment is "+ currencyFormatter.format(monthlyPayment));
//		    }
//	    }else {}//put them back to the customerMenu
//	  }

	public void calculator() {
		System.out.print("Loan Amount: ");
		double loanAmount = Koala.input.nextDouble();
		System.out.print("Number of Years: ");
		int years = Koala.input.nextInt();
		System.out.print("Annual Interest Rate: ");
		double annualRate = Koala.input.nextDouble();
		double monthlyRate = annualRate / 1200;
		double monthlyPayment = loanAmount * monthlyRate / (1 - 1 / Math.pow(1 + monthlyRate, years * 12));
		System.out.printf("Monthly Payment: %.2f\n", monthlyPayment);

		// Display total payment
		System.out.printf("Total Payment: %.2f\n", (monthlyPayment * 12) * years);

		// Create amortization schedule
		double balance = loanAmount, principal, interest;
		System.out.println("Payment#     Interest     Principal     Balance");
		for (int i = 1; i <= years * 12; i++) {
			interest = monthlyRate * balance;
			principal = monthlyPayment - interest;
			balance = balance - principal;
			System.out.printf("%-13d%-13.2f%-13.2f%.2f\n", i, interest, principal, balance);
		}
	}

}
