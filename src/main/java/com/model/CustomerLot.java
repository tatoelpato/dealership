package com.model;

import java.io.Serializable;

public class CustomerLot implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String VIN;
	private String username;
	private String manufacturer;
	private String model;
	private double value;

	public CustomerLot(String VIN, String username, String manufacturer, String model, double value) {
		super();
		this.VIN = VIN;
		this.username = username;
		this.manufacturer = manufacturer;
		this.model = model;
		this.value = value;
	}

	public String getVIN() {
		return VIN;
	}

	public void setVIN(String VIN) {
		this.VIN = VIN;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "VIN: " + VIN + " USERNAME: " + username + " MANUFACTURER: " + manufacturer + " MODEL: "
				+ model + " VALUE: $" + value;
	}

}
