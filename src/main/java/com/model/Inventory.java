package com.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class Inventory {

	private String carFile = "./src/main/java/com/data/CarLot.txt";
	private Map<String, Car> car;
	private ObjectOutputStream oos;
	private ObjectInputStream ois;

	public Inventory() {
		super();
		car = new TreeMap<String, Car>();
		File fl = new File(carFile);
		if (!fl.exists()) {
			try {
				fl.createNewFile();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		loadVehicle();
	}

	public boolean addVehicle(Car c) {
		if (car.containsKey(c.getVIN())) {
			return false;
		} else {
			car.put(c.getVIN(), c);
			return true;
		}
	}

	public void saveVehicle() {
		try {
			oos = new ObjectOutputStream(new FileOutputStream(carFile));
			oos.writeObject(car);
			oos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public Map<String, Car> loadVehicle() {
		try {
			ois = new ObjectInputStream(new FileInputStream(carFile));
			car = (Map<String, Car>) ois.readObject();
			ois.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return car;
	}

	public String printCar(String key) {
		return car.get(key).toString();
	}

	public boolean removeCar(String vin) {
		if (car.containsKey(vin)) {
			car.remove(vin);
			return true;
		} else {
			return false;
		}
	}

	public boolean vinExists(String vin) {
		return car.containsKey(vin);
	}

	public String printAll() {
		String ret = "";
		Set<String> s = car.keySet();
		for (String str : s) {
			ret += car.get(str).toString() + "\n";
		}
		return ret;
	}

}
