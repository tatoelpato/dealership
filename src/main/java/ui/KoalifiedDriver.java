package ui;

import java.util.logging.Logger;

import com.serialization.Koala;

public class KoalifiedDriver {
	
	final static Logger logger = Logger.getLogger("KoalifiedDriver");
	
	public static void main(String[] args) {
		Koala k = new Koala();
		k.launch();
		Koala.input.close();
	}
	
}
