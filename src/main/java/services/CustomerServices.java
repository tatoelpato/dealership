package services;

import com.model.CreatePayment;
import com.model.Inventory;
import com.serialization.Koala;
import com.serialization.Marketplace;

public class CustomerServices {

	private static Inventory i = new Inventory();

	public static void customerServices(String username) {
		int menuOption = 0;
		System.out.println(
				"Welcome " + username + " to the Dealership please pick from the following options (Press 0 for menu)");
		displayMenu();
		do {
			System.out.println("\n\nWhat would you like to do? (0 for main menu)");
			menuOption = Koala.input.nextInt();
			while (menuOption < 0 || menuOption > 4) {
				System.out.println("Please enter a correct menu option.(0 to see the menu)");
				menuOption = Koala.input.nextInt();
			}
			doOption(menuOption);

		} while (menuOption != 4);
	}

	public static void displayMenu() {
		System.out.println("\n1. View cars");
		System.out.println("2. Enter the Marketplace");
		System.out.println("3. My lot");
		System.out.println("4. Exit");
	}

	public static void doOption(int menu) {
		Koala k = new Koala();
		switch (menu) {
		case 0:
			System.out.println("Main Menu");
			displayMenu();
			break;
		case 1:
			System.out.println("List of Cars");
			showCars();
			break;
		case 2:
			System.out.println("Welcome to the marketplace");
			Marketplace.market();
			break;
		case 3:
			System.out.println("View my lot");
			myCars();
			break;
		default:
			System.out.println("Exit");
			k.launch();
			break;
		}
	}

	private static void myCars() {
		Marketplace.viewMyCar();
	}

	public static void showCars() {
		System.out.println(i.printAll());
	}

}
