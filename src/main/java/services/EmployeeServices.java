package services;

import com.model.Car;
import com.model.CreatePayment;
import com.model.Inventory;
import com.model.OfferMarket;
import com.serialization.Koala;

public class EmployeeServices {

	private static Inventory i = new Inventory();
	private static CreatePayment cP = new CreatePayment();
	private static OfferMarket oM = new OfferMarket();

	public static void employeeServices(String username) {
		int menuOption = 0;
		System.out.println("Welcome " + username + " Please pick from the " + "following options (Press 0 for menu)");
		displayMenu();
		do {
			System.out.println("\n\nWhat would you like to do? (0 for main menu)");
			menuOption = Koala.input.nextInt();
			while (menuOption < 0 || menuOption > 6) {
				System.out.println("Please enter a correct menu option.(0 to see the menu)");
				menuOption = Koala.input.nextInt();
			}
			doOption(menuOption);

		} while (menuOption != 6);
	}

	public static void displayMenu() {
		System.out.println("\n1. View cars");
		System.out.println("2. Add a car");
		System.out.println("3. Remove a car");
		System.out.println("4. View payments");
		System.out.println("5. Offers");
		System.out.println("6. Exit");
	}

	@SuppressWarnings("static-access")
	public static void doOption(int menu) {
		Koala k = new Koala();
		switch (menu) {
		case 0:
			System.out.println("Main Menu");
			displayMenu();
			break;
		case 1:
			System.out.println("List of Cars");
			showCars();
			break;
		case 2:
			System.out.println("Add a new car");
			addCar();
			break;
		case 3:
			System.out.println("Remove a car");
			removeCar();
			break;
		case 4:
			System.out.println("View payments");
			payments();
			break;
		case 5:
			System.out.println("View offers");
			oM.offers();
			break;
		default:
			System.out.println("Goodbye!");
			k.launch();
			break;
		}
	}

	private static void payments() {
		System.out.println(cP.printPayments());
	}

	private static void showCars() {
		System.out.println(i.printAll());
	}

	public static void addCar() {
		boolean validInput;
		int year = 0;
		String make;
		String model;
		int mileage = 0;
		double price = 0.0;
		String VIN;
		String color;
		do {
			validInput = true;
			System.out.println("Enter the car's year ");
			if (Koala.input.hasNextInt())
				year = Koala.input.nextInt();
			else
				validInput = false;
			System.out.println("Enter the car's manufacturer: ");
			make = Koala.input.next();
			System.out.println("Enter the car's model: ");
			model = Koala.input.next();
			System.out.println("Enter the car's mileage: ");
			if (Koala.input.hasNextInt())
				mileage = Koala.input.nextInt();
			else
				validInput = false;
			System.out.println("Enter the car's price: ");
			if (Koala.input.hasNextDouble())
				price = Koala.input.nextDouble();
			else
				validInput = false;
			System.out.println("Enter the car's VIN: (Must be 6 alpahnumeric characters) ");
			VIN = Koala.input.next().toUpperCase();
			VIN = VIN.toUpperCase();
			if (VIN.length() != 6) {
				validInput = false;
				System.out.println("VIN must be 6 characters");
			}
			System.out.println("Enter the car's color: ");
			color = Koala.input.next();
			if (!validInput) {
				System.out.println("\nIncorrect Format");
				System.out.println("Please try again\n");
			}
		} while (validInput == false);
		i.addVehicle(new Car(year, make, model, mileage, price, VIN, color));
		i.saveVehicle();
		System.out.println("Car has been added");
	}

	@SuppressWarnings("unused")
	public static void removeCar() {
		boolean validInput;
		int year = 0;
		String make;
		String model;
		int mileage = 0;
		double price = 0.0;
		String VIN;
		String color;
		do {
			System.out.println(i.printAll());
			validInput = true;
			System.out.println("Enter the car's year ");
			if (Koala.input.hasNextInt())
				year = Koala.input.nextInt();
			else
				validInput = false;
			System.out.println("Enter the car's manufacturer: ");
			make = Koala.input.next();
			System.out.println("Enter the car's model: ");
			model = Koala.input.next();
			System.out.println("Enter the car's mileage: ");
			if (Koala.input.hasNextInt())
				mileage = Koala.input.nextInt();
			else
				validInput = false;
			System.out.println("Enter the car's price: ");
			if (Koala.input.hasNextDouble())
				price = Koala.input.nextDouble();
			else
				validInput = false;
			System.out.println("Enter the car's VIN: (Must be 6 alpahnumeric characters) ");
			VIN = Koala.input.next().toUpperCase();
			VIN = VIN.toUpperCase();
			if (VIN.length() != 6) {
				validInput = false;
				System.out.println("VIN must be 6 characters");
			}
			System.out.println("Enter the car's color: ");
			color = Koala.input.next();
			if (!validInput) {
				System.out.println("\nIncorrect Format");
				System.out.println("Please try again\n");
			}
		} while (validInput == false);
		i.removeCar(VIN);
		i.saveVehicle();
		System.out.println("Car has been removed");
	}

}